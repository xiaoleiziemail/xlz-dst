package com.xlz.domain;

/**
 * 应用实例信息实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class AppInstance  extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appNo;
	private String registerInstanceId;
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}
	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}
	@Override
	public String toString() {
		return "AppInstance [id=" + id + ", appNo=" + appNo + ", registerInstanceIp=" + registerInstanceId
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
