package com.xlz.domain;

import java.util.Date;

/**
 * 加载统计映射实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class StatisticSelect extends BaseDomain {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appNo;
	private Long taskId;
	private String registerInstanceId;
	private String registerWorker;
	private Long loadDataTime;
	private Long loadDataTotal;
	private Integer loadDataFlag;
	private String errorContent;
	private Date createTime;

	public StatisticSelect(String appNo, Long taskId,String registerWorker,
			Long loadDataTime, Long loadDataTotal, Integer loadDataFlag, String errorContent) {
		super();
		this.appNo = appNo;
		this.taskId = taskId;
		this.registerWorker = registerWorker;
		this.loadDataTime = loadDataTime;
		this.loadDataTotal = loadDataTotal;
		this.loadDataFlag = loadDataFlag;
		this.errorContent = errorContent;
	}

	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getRegisterInstanceId() {
		return registerInstanceId;
	}

	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}

	public String getRegisterWorker() {
		return registerWorker;
	}

	public void setRegisterWorker(String registerWorker) {
		this.registerWorker = registerWorker;
	}

	public Long getLoadDataTime() {
		return loadDataTime;
	}

	public void setLoadDataTime(Long loadDataTime) {
		this.loadDataTime = loadDataTime;
	}

	public Long getLoadDataTotal() {
		return loadDataTotal;
	}

	public void setLoadDataTotal(Long loadDataTotal) {
		this.loadDataTotal = loadDataTotal;
	}

	public Integer getLoadDataFlag() {
		return loadDataFlag;
	}

	public void setLoadDataFlag(Integer loadDataFlag) {
		this.loadDataFlag = loadDataFlag;
	}

	public String getErrorContent() {
		return errorContent;
	}

	public void setErrorContent(String errorContent) {
		this.errorContent = errorContent;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "StatisticSelect [appNo=" + appNo + ", taskId=" + taskId + ", registerInstanceId=" + registerInstanceId
				+ ", registerWorker=" + registerWorker + ", loadDataTime=" + loadDataTime + ", loadDataTotal="
				+ loadDataTotal + ", loadDataFlag=" + loadDataFlag + ", errorContent=" + errorContent + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}

}
