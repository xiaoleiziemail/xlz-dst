package com.xlz.domain;

import java.util.Date;

/**
 * 应用工作者实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class AppWorker  extends BaseDomain{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long taskId              ;
	private String appNo ;
	private String registerInstanceId ;
	private String registerWorker   ;
	private Date createTime      ;
	private Date updateTime      ;
	private String shardItem           ;
	private Date loadTime            ;
	private Date executeTime         ;
	private Long loadTotal           ;
	private Long executeTotal        ;
	private Long waitDealTotal      ;
	private Integer active;
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}
	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}
	public String getRegisterWorker() {
		return registerWorker;
	}
	public void setRegisterWorker(String registerWorker) {
		this.registerWorker = registerWorker;
	}
	public String getShardItem() {
		return shardItem;
	}
	public void setShardItem(String shardItem) {
		this.shardItem = shardItem;
	}
	public Date getLoadTime() {
		return loadTime;
	}
	public void setLoadTime(Date loadTime) {
		this.loadTime = loadTime;
	}
	public Date getExecuteTime() {
		return executeTime;
	}
	public void setExecuteTime(Date executeTime) {
		this.executeTime = executeTime;
	}
	public Long getLoadTotal() {
		return loadTotal;
	}
	public void setLoadTotal(Long loadTotal) {
		this.loadTotal = loadTotal;
	}
	public Long getExecuteTotal() {
		return executeTotal;
	}
	public void setExecuteTotal(Long executeTotal) {
		this.executeTotal = executeTotal;
	}
	public Long getWaitDealTotal() {
		return waitDealTotal;
	}
	public void setWaitDealTotal(Long waitDealTotal) {
		this.waitDealTotal = waitDealTotal;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	@Override
	public String toString() {
		return "AppWorker [id=" + id + ", taskId=" + taskId + ", appNo=" + appNo + ", registerInstanceId="
				+ registerInstanceId + ", registerWorker=" + registerWorker + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", shardItem=" + shardItem + ", loadTime=" + loadTime
				+ ", executeTime=" + executeTime + ", loadTotal=" + loadTotal + ", executeTotal=" + executeTotal
				+ ", waitDealTotal=" + waitDealTotal + ", active=" + active + "]";
	}

}
