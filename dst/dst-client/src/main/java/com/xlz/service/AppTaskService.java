package com.xlz.service;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.xlz.domain.AppTask;

/**
 * 应用任务实例信息CRUD工具类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class AppTaskService extends BaseJdbcService<AppTask>{
	
	public AppTaskService(DataSource dataSource) {
		super(dataSource);
	}

	public int updateReadinessById(Long id,int readiness) throws SQLException{
		// 发送心跳
		String sql = "update app_task set update_time=now(),readiness= " + readiness + " where readiness="+(readiness==1?0:1)+" and id=" + id;
		return executeUpdate(sql);
	} 
	
	public int updateActiveByAppNo(String appNo,int readiness) throws SQLException{
		// 发送心跳
		String sql = "update app_task set update_time=now(),readiness= " + readiness + " where readiness=1 and app_no='" + appNo +"'";
		return executeUpdate(sql);
	} 
	
	public List<AppTask> getAllByAppNo(String appNo) {
		String sql = "select "
				+    	"id ,task_name,deal_bean,cron_expression,task_param ,fetch_data_number,task_group_count ,single_task_thread_count ,shard_item ,bind_ip,active ,task_backlog_data,app_no ,readiness,execute_method,update_time,dead_time "
				+ 	 "from app_task "
				+ 	 "where app_no='"+appNo+"'";// and active=1";
		return findAll(sql, AppTask.class);
	}
	
	public AppTask getByID(Long id) {
		String sql = "select "
				+    	"id ,task_name,deal_bean,cron_expression,task_param ,fetch_data_number,task_group_count ,single_task_thread_count ,shard_item ,bind_ip,active ,task_backlog_data,app_no ,readiness,execute_method,update_time,dead_time "
				+ 	 "from app_task "
				+ 	 "where id="+id;
		try {
			return find(sql, AppTask.class);
		} catch (Exception e) {
			LOG.error("获取任务信息异常：{}",id,e);
			return null;
		}
	}

	@Override
	public String getNamespace() {
		return "com.xlz.pojo.AppTask.";
	}
}
