package com.xlz.service;

import java.util.List;

import javax.sql.DataSource;

import com.xlz.domain.App;

/**
 * 应用信息CRUD工具类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class AppService extends BaseJdbcService<App>{

	
	public AppService(DataSource dataSource) {
		super(dataSource);
	}

	public App getByAppNo(String appNo) {
		String sql = "select "
				+    	"id ,app_name,app_no,heart_beat_rate,dead_heart_count,create_time,update_time,now() 'current_time' "
				+ 	 "from app "
				+ 	 "where active=1 and app_no='"+appNo+"'";
		List<App> list = findAll(sql, App.class);
		if(list.size()==1){
			return list.get(0);
		}
		return null;
	}

	@Override
	public String getNamespace() {
		return "com.xlz.pojo.App.";
	}
}
