package com.xlz.statistics;

import java.util.concurrent.atomic.AtomicLong;

import com.xlz.domain.StatisticExecute;
import com.xlz.manager.DefaultDstManager;

/**
 * 执行统计缓存工具类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class ExecuteStatisticCache {
	private DefaultDstManager scheduleManager;

	private AtomicLong executeSuccessTotal = new AtomicLong(0);// 处理成功的数据量
	private AtomicLong executeFailTotal = new AtomicLong(0);// 处理失败的数据量
	private AtomicLong executeDataTime = new AtomicLong(0);// 处理总耗时

	public void addExecuteSuccessTotal(long value) {
		this.executeSuccessTotal.addAndGet(value);
	}

	public void addExecuteFailTotal(long value) {
		this.executeFailTotal.addAndGet(value);
	}

	public void addExecuteDataTime(long value) {
		this.executeDataTime.addAndGet(value);
	}

	public long getExecuteSuccessTotal() {
		return executeSuccessTotal.getAndSet(0);
	}

	public long getExecuteFailTotal() {
		return executeFailTotal.getAndSet(0);
	}

	public long getExecuteDataTime() {
		return executeDataTime.getAndSet(0);
	}

	public void flushToDb(Long taskId, String registerWorker) {
		StatisticExecute entity = new StatisticExecute();
		entity.setAppNo(scheduleManager.getContext().getAppNo());
		entity.setTaskId(taskId);
		entity.setRegisterInstanceId(scheduleManager.getContext().getInstanceId());
		entity.setRegisterWorker(registerWorker);
		entity.setExecuteDataTime(this.getExecuteDataTime());
		entity.setExecuteSuccessTotal(this.getExecuteSuccessTotal());
		entity.setExecuteFailTotal(this.getExecuteFailTotal());
		scheduleManager.getStatisticExecuteService().insert(entity);
	}

	public void setScheduleManager(DefaultDstManager scheduleManager) {
		this.scheduleManager = scheduleManager;
	}

}