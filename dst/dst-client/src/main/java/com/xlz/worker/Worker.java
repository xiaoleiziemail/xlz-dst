package com.xlz.worker;

import com.xlz.domain.AppTask;
import com.xlz.manager.DefaultDstManager;

/**
 * 分布式调度工作组模板.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public interface Worker {

	void init();
	
	void doSelect();
	
	void doExecute();
	
	void setShardItems(String currShardItems);
	
	AppTask getAppTask();
	
	void setAppTask(AppTask appTask);
	
	void setThreadPoolSize(int threadPoolSize);
	
	void setShardItemCount(Integer shardItemCount);
	
	void setRegisterWorker(String registerWorker);
	
	void setScheduleManager(DefaultDstManager dstManager);
	
	void flushStatistic();

	void shutdownNow();

	void shutdown();
	
	boolean isStoped();
	
	boolean isRunning();
}
