package com.xlz.worker;

import org.springframework.context.ApplicationContext;

/**
 * 分布式调度工作组工厂.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class WorkerBeanFactory {

	public static ApplicationContext context;
	
	public static Worker newWorker(String dealBean){
		return context.getBean(dealBean, Worker.class);
	}
	
}
