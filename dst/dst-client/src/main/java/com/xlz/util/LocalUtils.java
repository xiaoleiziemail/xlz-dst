package com.xlz.util;

import java.net.InetAddress;

public class LocalUtils {
	public static String getLocalIP() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (Exception e) {
			return "";
		}
	}
}
