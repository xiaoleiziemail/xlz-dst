package com.xlz.schedule.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.service.impl.BaseServiceImpl;
import com.xlz.schedule.dao.AppTaskDao;
import com.xlz.schedule.domain.AppTask;
import com.xlz.schedule.service.AppTaskService;

/**
 * Created by zhangleilei
 */
@Service
public class AppTaskServiceImpl extends BaseServiceImpl<AppTask> implements AppTaskService{

	@Autowired
	private AppTaskDao scheduleTaskDao;
	
	@Override
	protected BaseDao<AppTask> getDAO() {
		return scheduleTaskDao;
	}

	@Override
	public List<Map<String, Object>> findAppTaskChartData() {
		return scheduleTaskDao.findAppTaskChartData();
	}

}
