package com.xlz.schedule.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlz.common.annotation.Log;
import com.xlz.common.controller.BaseController;
import com.xlz.common.datasource.DatabaseContextHolder;
import com.xlz.common.datasource.DatabaseType;
import com.xlz.common.utils.PageUtils;
import com.xlz.common.utils.Query;
import com.xlz.common.utils.R;
import com.xlz.schedule.domain.AppTask;
import com.xlz.schedule.service.AppTaskService;
import com.xlz.system.service.DeptService;

/**
 * Created by zhangleilei 
 */
@Controller
@RequestMapping("/schedule/task")
public class AppTaskController extends BaseController {
	String prefix = "schedule/task";
	
    @Resource
    private AppTaskService appTaskService;
    @Resource
    private DeptService deptService;

    @RequiresPermissions("schedule:task:main")
	@GetMapping()
	String role() {
		return prefix + "/main";
	}

    @GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);
		DatabaseContextHolder.setDatabaseType(DatabaseType.schedule);
		List<AppTask> list = appTaskService.list(query);
		int total = appTaskService.count(query);
		PageUtils pageUtil = new PageUtils(list, total);
		return pageUtil;
	}

	@Log("添加任务")
	@RequiresPermissions("schedule:task:add")
	@GetMapping("/add")
	String add() {
		return prefix + "/add";
	}

	@Log("编辑任务")
	@RequiresPermissions("schedule:task:edit")
	@GetMapping("/edit/{id}")
	String edit(@PathVariable("id") Long id, Model model) {
		AppTask entity = appTaskService.get(id);
		model.addAttribute("entity", entity);
		return prefix + "/edit";
	}

	@Log("保存任务")
	@RequiresPermissions("schedule:task:add")
	@PostMapping("/save")
	@ResponseBody()
	R save(AppTask entity) {
		entity.setCreateUser(getUsername());
		if (appTaskService.save(entity) > 0) {
			return R.ok();
		} else {
			return R.error(1, "保存失败");
		}
	}

	@Log("更新任务")
	@RequiresPermissions("schedule:task:edit")
	@PostMapping("/update")
	@ResponseBody()
	R update(AppTask entity) {
		AppTask old = appTaskService.get(entity.getId());
		if(!old.getAppNo().equals(entity.getAppNo()) || 
				!old.getAppNo().equals(entity.getAppNo()) || 
				!old.getDealBean().equals(entity.getDealBean()) || 
				!old.getTaskGroupCount().equals(entity.getTaskGroupCount()) || 
				!old.getSingleTaskThreadCount().equals(entity.getSingleTaskThreadCount()) || 
				!old.getBindIp().equals(entity.getBindIp()) || 
				!old.getExecuteMethod().equals(entity.getExecuteMethod()) || 
				!old.getActive().equals(entity.getActive()) || 
				!old.getShardItem().equals(entity.getShardItem())){
			entity.setReadiness(0);
		}
		entity.setUpdateUser(getUsername());
		if (appTaskService.update(entity) > 0) {
			return R.ok();
		} else {
			return R.error(1, "保存失败");
		}
	}

	@Log("删除任务")
	@RequiresPermissions("schedule:task:remove")
	@PostMapping("/remove")
	@ResponseBody()
	R remove(Long id) {
		if (appTaskService.remove(id) > 0) {
			return R.ok();
		} else {
			return R.error(1, "删除失败");
		}
	}
	
	@RequiresPermissions("schedule:task:batchRemove")
	@Log("批量删除任务")
	@PostMapping("/batchRemove")
	@ResponseBody
	R batchRemove(@RequestParam("ids[]") Long[] ids) {
		int r = appTaskService.batchRemove(ids);
		if (r > 0) {
			return R.ok();
		}
		return R.error();
	}

}
