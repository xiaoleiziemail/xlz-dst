package com.xlz.schedule.domain;

import com.xlz.common.domain.BaseDO;

public class AppInstance extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 应用标识 */
	private String appNo;  
	/** 注册的实例id */
	private String registerInstanceId;  
  	
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}

	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}

}