package com.xlz.schedule.dao;

import java.util.List;
import java.util.Map;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.StatisticExecute;
/**
 * @author ������
 */
public interface StatisticExecuteDao extends BaseDao<StatisticExecute> {
	
	List<Map> executeDataTotal(Query query);
	
	List<Map> executeDataTime(Query query);
}