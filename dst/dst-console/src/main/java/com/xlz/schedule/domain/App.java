package com.xlz.schedule.domain;

import com.xlz.common.domain.BaseDO;

public class App extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 应用编号 */
	private String appNo;  
	/** 应用名 */
	private String appName;  
	/** 心跳时间 */
	private String heartBeatRate;  
	/** 可用状态 */
	private Integer active;  
	/** 死亡心跳个数，最少5个 */
	private Integer deadHeartCount;  
	/** 部门id */
	private Long deptId;  
	private String deptName;
  	
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getHeartBeatRate() {
		return heartBeatRate;
	}

	public void setHeartBeatRate(String heartBeatRate) {
		this.heartBeatRate = heartBeatRate;
	}
	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
	public Integer getDeadHeartCount() {
		return deadHeartCount;
	}

	public void setDeadHeartCount(Integer deadHeartCount) {
		this.deadHeartCount = deadHeartCount;
	}
	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

}