package com.xlz.schedule.domain;

import com.xlz.common.domain.BaseDO;

public class StatisticExecute extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 应用编号 */
	private String appNo;  
	/** 任务id */
	private String taskId;  
	/** 注册的实例id */
	private String registerInstanceId;  
	/** 注册的任务组 */
	private String registerWorker;  
	/** 处理数据时间（最近一次心跳上传） */
	private String executeDataTime;  
	/** 执行成功的记录总条数 */
	private String executeSuccessTotal;  
	/** 执行失败的记录总条数 */
	private String executeFailTotal;  
  	
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}

	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}
	public String getRegisterWorker() {
		return registerWorker;
	}

	public void setRegisterWorker(String registerWorker) {
		this.registerWorker = registerWorker;
	}

	public String getExecuteDataTime() {
		return executeDataTime;
	}

	public void setExecuteDataTime(String executeDataTime) {
		this.executeDataTime = executeDataTime;
	}
	public String getExecuteSuccessTotal() {
		return executeSuccessTotal;
	}

	public void setExecuteSuccessTotal(String executeSuccessTotal) {
		this.executeSuccessTotal = executeSuccessTotal;
	}
	public String getExecuteFailTotal() {
		return executeFailTotal;
	}

	public void setExecuteFailTotal(String executeFailTotal) {
		this.executeFailTotal = executeFailTotal;
	}

}