package com.xlz.schedule.dao;

import java.util.List;
import java.util.Map;

import com.xlz.common.dao.BaseDao;
import com.xlz.schedule.domain.AppTask;
/**
 * @author ������
 */
public interface AppTaskDao extends BaseDao<AppTask> {

	List<Map<String, Object>> findAppTaskChartData();
	
}