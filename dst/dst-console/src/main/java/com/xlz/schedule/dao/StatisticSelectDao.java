package com.xlz.schedule.dao;

import java.util.List;
import java.util.Map;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.StatisticSelect;
/**
 * @author ������
 */
public interface StatisticSelectDao extends BaseDao<StatisticSelect> {

	List<Map> selectDataTotal(Query query);
	
	List<Map> selectDataTime(Query query);
	
}