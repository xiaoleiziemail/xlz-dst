package com.xlz.schedule.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.service.impl.BaseServiceImpl;
import com.xlz.schedule.dao.AppWorkerDao;
import com.xlz.schedule.domain.AppWorker;
import com.xlz.schedule.domain.AppTask;
import com.xlz.schedule.service.AppWorkerService;

/**
 * Created by zhangleilei
 */
@Service
public class AppWorkerServiceImpl extends BaseServiceImpl<AppWorker> implements AppWorkerService{

	@Autowired
	private AppWorkerDao appWorkerDao;
	
	@Override
	protected BaseDao<AppWorker> getDAO() {
		return appWorkerDao;
	}

}
