package com.xlz.schedule.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlz.common.controller.BaseController;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.AppInstance;
import com.xlz.schedule.service.AppInstanceService;
import com.xlz.schedule.service.AppTaskService;
import com.xlz.schedule.service.StatisticExecuteService;
import com.xlz.schedule.service.StatisticSelectService;

/**
 * Created by zhangleilei 
 */
@Controller
@RequestMapping("/schedule/chart")
public class StatisticChartController extends BaseController {
	String prefix = "schedule/chart";
	
    @Resource
    private AppTaskService appTaskService;
    @Resource
    private AppInstanceService appInstanceService;
    @Resource
    private StatisticSelectService statisticSelectService;
    @Resource
    private StatisticExecuteService statisticExecuteService;

    @RequiresPermissions("schedule:chart:apptask")
	@GetMapping("/apptask")
	String apptask() {
		return prefix + "/apptask";
	}

    @GetMapping("/apptaskData")
	@ResponseBody
	Map<String ,Object> apptaskData(@RequestParam Map<String, Object> params) {
		// 查询列表数据
    	List<Map<String,Object>> list = appTaskService.findAppTaskChartData();
    	Map<String ,Object> result = new HashMap<>();
    	List<String> legendData = new ArrayList<>(list.size());
    	List<Map<String,Object>> seriesData = new ArrayList<>(list.size());
    	List<Boolean> selected = new ArrayList<>(list.size());
    	for(int i = 0;i < list.size();i ++){
    		String appNo = list.get(i).get("app_no").toString();
    		legendData.add(appNo);
    		Map<String,Object> item = new HashMap<>();
    		item.put("name", appNo);
    		item.put("value", list.get(i).get("nums"));
    		seriesData.add(item);
    		selected.add(true);
    	}
    	result.put("legendData", legendData);
    	result.put("seriesData", seriesData);
    	result.put("selected", selected);
		return result;
	}

    @RequiresPermissions("schedule:chart:instance")
	@GetMapping("/instance")
	String instance() {
		return prefix + "/instance";
	}

    @GetMapping("/instanceData")
	@ResponseBody
	Map<String ,Object> instanceData(@RequestParam Map<String, Object> params) {
		// 查询列表数据
    	Query query = new Query(params);
    	query.put("sort", "app_no,id");
		query.put("order", "desc");
    	List<AppInstance> list = appInstanceService.list(query);
    	Map<String ,Object> result = new HashMap<>();
    	result.put("name", "应用实例分布");
    	List children = new ArrayList<>();
    	result.put("children", children);
    	
    	String appNo = "";
    	List children1 = null;
    	for(AppInstance entity : list){
    		if(!appNo.equals(entity.getAppNo())){
    			appNo = entity.getAppNo();
    			Map<String ,Object> mm = new HashMap<>();
    			children1 = new ArrayList<>();
    			mm.put("name", entity.getAppNo() + "(Master["+entity.getId()+"]:" + getIpFromGroupId(entity.getRegisterInstanceId()) + ")");
    			mm.put("children", children1);
    			children.add(mm);
    		}else{
    			Map<String ,String> item = new HashMap<>();
    			item.put("name", getIpFromGroupId(entity.getRegisterInstanceId()));
    			item.put("value", entity.getId().toString());
    			children1.add(item);
    		}
    		
    	}
		return result;
	}
    
    @RequiresPermissions("schedule:chart:select")
	@GetMapping("/select")
	String select() {
		return prefix + "/select";
	}

    @GetMapping("/selectDataTotal")
	@ResponseBody
	Map<String ,Object> selectDataTotal(@RequestParam Map<String, Object> params,Integer step) {
		// 查询列表数据
    	Query query = new Query(params);
    	List<Map> list = statisticSelectService.selectDataTotal(query);
    	Map<String ,Object> result = new HashMap<>();
    	List<Object> xList = new ArrayList<>();
    	result.put("x", xList);
    	List<Object> yList = new ArrayList<>();
    	result.put("y", yList);
    	for(Map entity : list){
    		xList.add(entity.get("ymd")+" "+entity.get("h")+":"+ (((Integer)entity.get("m5")) * step));
    		yList.add(entity.get("y"));
    	}
		return result;
	}
    
    @GetMapping("/selectDataTime")
    @ResponseBody
    Map<String ,Object> selectDataTime(@RequestParam Map<String, Object> params,Integer step) {
    	// 查询列表数据
    	Query query = new Query(params);
    	List<Map> list = statisticSelectService.selectDataTime(query);
    	Map<String ,Object> result = new HashMap<>();
    	List<Object> xList = new ArrayList<>();
    	result.put("x", xList);
    	List<Object> yList = new ArrayList<>();
    	result.put("y", yList);
    	for(Map entity : list){
    		xList.add(entity.get("ymd")+" "+entity.get("h")+":"+ (((Integer)entity.get("m5")) * step));
    		yList.add(entity.get("y"));
    	}
    	return result;
    }
    
    private String getIpFromGroupId(String groupId){
    	return groupId.substring(0,groupId.indexOf("$"));
    }
    
    @RequiresPermissions("schedule:chart:execute")
    @GetMapping("/execute")
    String execute() {
    	return prefix + "/execute";
    }
    
    @GetMapping("/executeDataTotal")
    @ResponseBody
    Map<String ,Object> executeDataTotal(@RequestParam Map<String, Object> params,Integer step) {
    	// 查询列表数据
    	Query query = new Query(params);
    	List<Map> list = statisticExecuteService.executeDataTotal(query);
    	Map<String ,Object> result = new HashMap<>();
    	List<Object> xList = new ArrayList<>();
    	result.put("x", xList);
    	List<Object> yList = new ArrayList<>();
    	result.put("y", yList);
    	for(Map entity : list){
    		xList.add(entity.get("ymd")+" "+entity.get("h")+":"+ (((Integer)entity.get("m5")) * step));
    		yList.add(entity.get("y"));
    	}
    	return result;
    }
    
    @GetMapping("/executeDataTime")
    @ResponseBody
    Map<String ,Object> executeDataTime(@RequestParam Map<String, Object> params,Integer step) {
    	// 查询列表数据
    	Query query = new Query(params);
    	List<Map> list = statisticExecuteService.executeDataTime(query);
    	Map<String ,Object> result = new HashMap<>();
    	List<Object> xList = new ArrayList<>();
    	result.put("x", xList);
    	List<Object> yList = new ArrayList<>();
    	result.put("y", yList);
    	for(Map entity : list){
    		xList.add(entity.get("ymd")+" "+entity.get("h")+":"+ (((Integer)entity.get("m5")) * step));
    		yList.add(entity.get("y"));
    	}
    	return result;
    }
}
