package com.xlz.schedule.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.service.impl.BaseServiceImpl;
import com.xlz.schedule.dao.AppDao;
import com.xlz.schedule.domain.App;
import com.xlz.schedule.service.AppService;

/**
 * Created by zhangleilei
 */
@Service
public class AppServiceImpl extends BaseServiceImpl<App> implements AppService{

	@Autowired
	private AppDao scheduleAppDao;
	
	@Override
	protected BaseDao<App> getDAO() {
		return scheduleAppDao;
	}

}
