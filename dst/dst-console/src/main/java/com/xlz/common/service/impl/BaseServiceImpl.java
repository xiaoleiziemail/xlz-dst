package com.xlz.common.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.domain.BaseDO;
import com.xlz.common.service.BaseService;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public abstract class BaseServiceImpl<T extends BaseDO> implements BaseService<T>{
	/**
	 * 日志
	 */
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	
	protected abstract BaseDao<T> getDAO();

	@Override
	public T get(Long id) {
		T t = getDAO().get(id);
		return t;
	}

	@Override
	public List<T> list(Map<String, Object> map) {
		return getDAO().list(map);
	}

	@Override
	public int count(Map<String, Object> map) {
		return getDAO().count(map);
	}

	@Transactional
	@Override
	public int save(T t) {
		int count = getDAO().save(t);
		return count;
	}

	@Override
	public int update(T t) {
		int r = getDAO().update(t);
		return r;
	}

	@Override
	public int remove(Long tId) {
		return getDAO().remove(tId);
	}
	
	@Override
    public int batchRemove(Long[] ids) {
        return getDAO().batchRemove(ids);
    }
}
