package com.xlz.common.controller;

import org.springframework.stereotype.Controller;

import com.xlz.common.utils.ShiroUtils;
import com.xlz.system.domain.UserDO;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
@Controller
public class BaseController {
	public UserDO getUser() {
		return ShiroUtils.getUser();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}

	public String getUsername() {
		return getUser().getUsername();
	}
	
}