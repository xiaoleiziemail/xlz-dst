package com.xlz.common.dao;

import java.util.List;
import java.util.Map;

import com.xlz.common.domain.BaseDO;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public interface BaseDao<T extends BaseDO> {

    void delete(Long id);

    T findById(Long id);
    
    T get(Long tId);
	
	List<T> list(Map<String,Object> map);
	
	int count(Map<String,Object> map);
	
	int save(T t);
	
	int update(T t);
	
	int remove(Long tId);
	
	int batchRemove(Long[] ids);
}
