package com.xlz.common.service;

import org.springframework.stereotype.Service;

import com.xlz.common.domain.LogDO;
import com.xlz.common.domain.PageDO;
import com.xlz.common.utils.Query;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
@Service
public interface LogService {
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
