package com.xlz.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.xlz.common.aspect.DataSourceInterceptor;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
@Component
class WebConfigurer extends WebMvcConfigurerAdapter {
	@Autowired
	DstConfig dstConfig;
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/files/**").addResourceLocations("file:///"+dstConfig.getUploadPath());
	}

	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new DataSourceInterceptor()).addPathPatterns("/schedule/**");
        registry.addInterceptor(new DataSourceInterceptor()).addPathPatterns("/statstic/**");
        super.addInterceptors(registry);
    }
}