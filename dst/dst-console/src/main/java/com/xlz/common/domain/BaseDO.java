package com.xlz.common.domain;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class BaseDO implements Serializable {

	private static final long serialVersionUID = -4462320355563185956L;
	
	/** 主键id */
	protected Long id;

	/** 创建人 */
	protected String createUser;

	/** 创建时间 */
	protected Date createTime;

	/** 更新人 */
	protected String updateUser;

	/** 更新时间 */
	protected Date updateTime;

	/** 状态  */
	protected Integer active;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

}
