package com.xlz.system.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.xlz.system.domain.RoleDO;
/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
@Service
public interface RoleService {

	RoleDO get(Long id);

	List<RoleDO> list();

	int save(RoleDO role);

	int update(RoleDO role);

	int remove(Long id);

	List<RoleDO> list(Long userId);

	int batchremove(Long[] ids);
}
