package com.xlz.system.service;

import java.util.Collection;
import java.util.List;

import org.apache.shiro.session.Session;
import org.springframework.stereotype.Service;

import com.xlz.system.domain.UserDO;
import com.xlz.system.domain.UserOnline;
/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
@Service
public interface SessionService {
	List<UserOnline> list();

	List<UserDO> listOnlineUser();

	Collection<Session> sessionList();
	
	boolean forceLogout(String sessionId);
}
