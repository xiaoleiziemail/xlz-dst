var prefix = "/schedule/chart"
$(function() {
	var myChart = echarts.init(document.getElementById('echarts-chart'),"vintage");
	//myChart.setOption(option);
	myChart.showLoading();
	
	$.get(prefix + "/instanceData", function (data) {
	    myChart.hideLoading();
	    myChart.setOption(option = {
	            tooltip: {
	                trigger: 'item',
	                triggerOn: 'mousemove'
	            },
	            series: [
	                {
	                    type: 'tree',

	                    data: [data],

	                    top: '1%',
	                    left: '20%',
	                    bottom: '1%',
	                    right: '20%',

	                    symbolSize: 7,

	                    label: {
	                        normal: {
	                            position: 'left',
	                            verticalAlign: 'middle',
	                            align: 'right',
	                            fontSize: 12
	                        }
	                    },

	                    leaves: {
	                        label: {
	                            normal: {
	                                position: 'right',
	                                verticalAlign: 'middle',
	                                align: 'left'
	                            }
	                        }
	                    },

	                    expandAndCollapse: true,
	                    animationDuration: 550,
	                    animationDurationUpdate: 750
	                }
	            ]
	        });
	});
});
