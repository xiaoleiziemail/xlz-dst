var prefix = "/schedule/chart"
$(function() {
	$.datetimepicker.setLocale('ch');//设置中文
	//初始化时间控件
	$('#startTime').datetimepicker();
	
	//初始化图表
	var myChart = echarts.init(document.getElementById('echarts-chart'),"vintage");
	//初始化表格
	loadTable();
	
	$("#reLoad").click(function(){
		var taskId = $("#task").val();
		if(taskId == null || taskId == ""){
			return ;
		}
		var startTime = $("#startTime").val();
		if(startTime == null || startTime == ""){
			alert("请选择开始时间!");
			return ;
		}
		//加载图表
		myChart.showLoading();
		$.get(prefix + "/executeData"+$("#category").val(),
			{
				taskId:taskId,
				startTime:startTime,
				step:$("#step").val()
			}, function (data) {
			    myChart.hideLoading();
			    myChart.setOption(option = {
		    	    xAxis: {
		    	        type: 'category',
		    	        data: data.x
		    	    },
		    	    yAxis: {
		    	        type: 'value'
		    	    },
		    	    series: [{
		    	        data: data.y,
		    	        type: 'line'
		    	    }]
		    	});
			});
		//加载表格
		$('#exampleTable').bootstrapTable('refresh');
	});
	
	
	$('#app').change(function () {
		$("#task").empty();
		$.get("/schedule/task/list",{appNo:$("app").val()}, function (data) {
			$("#task").empty();
			$("#task").append("<option value=\"\">---请选择任务---</option>");
			$.each(data.rows, function (index, item) {  
				var id = item.id; 
				var text = item.taskName+"["+item.id+"]"; 
				$("#task").append("<option value='"+id+"'>"+text+"</option>");
			}); 
		});
	})
	
	
	/////////////////////////////////////////////////
	
	function loadTable() {
		$('#exampleTable')
			.bootstrapTable(
				{
					method : 'get', // 服务器数据的请求方式 get or post
					url : "/schedule/execute/list", // 服务器数据的加载地址
					// showRefresh : true,
					// showToggle : true,
					// showColumns : true,
					iconSize : 'outline',
					toolbar : '#exampleToolbar',
					striped : true, // 设置为true会有隔行变色效果
					dataType : "json", // 服务器返回的数据类型
					pagination : true, // 设置为true会在底部显示分页条
					// queryParamsType : "limit",
					// //设置为limit则会发送符合RESTFull格式的参数
					singleSelect : false, // 设置为true将禁止多选
					// contentType : "application/x-www-form-urlencoded",
					// //发送到服务器的数据编码类型
					pageSize : 10, // 如果设置了分页，每页数据条数
					pageNumber : 1, // 如果设置了分布，首页页码
					// search : true, // 是否显示搜索框
					showColumns : false, // 是否显示内容下拉框（选择显示的列）
					sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
					// "server"
					queryParams : function(params) {
						return {
							// 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
							limit : params.limit,
							offset : params.offset,
							taskId : $("#task").val(),
							startTime : $("#startTime").val()
						};
					},
					// //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
					// queryParamsType = 'limit' ,返回参数必须包含
					// limit, offset, search, sort, order 否则, 需要包含:
					// pageSize, pageNumber, searchText, sortName,
					// sortOrder.
					// 返回false将会终止请求
					columns : [
						{
							checkbox : true
						},
						{
							field : 'appNo',
							title : '应用编号'
						},
						{
							field : 'taskId',
							title : '任务ID'
						},
						{
							field : 'registerInstanceId',
							title : '注册的实例'
						},
						{
							field : 'registerWorker',
							title : '注册的任务组'
						},
						{
							field : 'executeDataTime',
							title : '处理数据平均时间'
						},
						{
							field : 'executeSuccessTotal',
							title : '本次成功条数'
						},
						{
							field : 'executeFailTotal',
							title : '本次失败条数'
						},
						{
							field : 'createTime',
							title : '上报时间'
						} ]
				});
	}
});

var openDept = function(){
	layer.open({
		type:2,
		title:"选择部门",
		area : [ '300px', '450px' ],
		content:"/system/sysDept/treeView"
	})
}
function loadDept( deptId,deptName){
	$("#deptId").val(deptId);
	$("#deptName").val(deptName);
	loadAppList(deptId)
}
//获取部门对应的应用
function loadAppList(deptId){
	$("#app").empty();
	$("#app").append("<option value=\"\">---请选择应用---</option>");
	$("#task").empty();
	$("#task").append("<option value=\"\">---请选择任务---</option>");
	$.get("/schedule/app/list",{deptId:deptId}, function (data) {
		$("#app").empty();
		$("#app").append("<option value=\"\">---请选择应用---</option>");
		$.each(data.rows, function (index, item) {  
            var id = item.id; 
            var text = item.appName+"["+item.appNo+"]"; 
            $("#app").append("<option value='"+id+"'>"+text+"</option>");
        }); 
	});
}