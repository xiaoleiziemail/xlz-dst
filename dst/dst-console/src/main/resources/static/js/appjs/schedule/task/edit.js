$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		update();
	}
});
function update() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/schedule/task/update",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			taskName : {
				required : true
			},
			cronExpression : {
				required : true
			},
			fetchDataNumber : {
				required : true,
				number : true
			},deadTime : {
				required : true,
				number : true,
				min : 20
			},
			taskGroupCount : {
				required : true,
				number : true,
				min : 1
			},
			singleTaskThreadCount : {
				required : true,
				number : true,
				min : 1
			},
			shardItem : {
				required : true
			},
			bindIp : {
				required : true
			}
		},
		messages : {
			taskName : {
				required : icon + "请输入任务名称"
			},
			cronExpression : {
				required : icon + "请输入Cron表达式或者数字时间（ms）"
			},
			fetchDataNumber : {
				required : icon + "请输入每次加载数据条数",
				number : "输入参数必须为数字"
			},
			deadTime : {
				required : icon + "请输入假死时间",
				number : icon + "输入只能为数字",
				min : "最小为20 s"
			},
			taskGroupCount : {
				required : icon + "请输入单任务线程组数",
				number : icon + "输入只能为数字",
				min : "最小为1"
			},
			singleTaskThreadCount : {
				required : icon + "请输入单任务组线程数",
				number : icon + "输入只能为数字",
				min : "最小为1"
			},
			shardItem : {
				required : icon + "请输入指定的分片"
			},
			bindIp : {
				required : icon + "请输入所绑定的ip"
			}
		}
	})
}
