$().ready(function() {
	validateRule();
});
$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/schedule/task/save",
		data : $('#signupForm').serialize(), // 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("网络超时");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name);
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			deptName : {
				required : true
			},
			appNo : {
				required : true
			},
			taskName : {
				required : true
			},
			cronExpression : {
				required : true
			},
			fetchDataNumber : {
				required : true,
				number : true
			},deadTime : {
				required : true,
				number : true,
				min : 20
			},
			taskGroupCount : {
				required : true,
				number : true,
				min : 1
			},
			singleTaskThreadCount : {
				required : true,
				number : true,
				min : 1
			},
			shardItem : {
				required : true
			},
			bindIp : {
				required : true
			}
		},
		messages : {
			deptName : {
				required : icon + "请选择所属部门"
			},
			appNo : {
				required : icon + "请选择所属的应用"
			},
			taskName : {
				required : icon + "请输入任务名称"
			},
			cronExpression : {
				required : icon + "请输入Cron表达式或者数字时间（ms）"
			},
			fetchDataNumber : {
				required : icon + "请输入每次加载数据条数",
				number : "输入参数必须为数字"
			},
			deadTime : {
				required : icon + "请输入假死时间",
				number : icon + "输入只能为数字",
				min : "最小为20 s"
			},
			taskGroupCount : {
				required : icon + "请输入单任务线程组数",
				number : icon + "输入只能为数字",
				min : "最小为1"
			},
			singleTaskThreadCount : {
				required : icon + "请输入单任务组线程数",
				number : icon + "输入只能为数字",
				min : "最小为1"
			},
			shardItem : {
				required : icon + "请输入指定的分片"
			},
			bindIp : {
				required : icon + "请输入所绑定的ip"
			}
		}
	})
}

var openDept = function(){
	layer.open({
		type:2,
		title:"选择部门",
		area : [ '300px', '450px' ],
		content:"/system/sysDept/treeView"
	})
}
function loadDept( deptId,deptName){
	$("#deptId").val(deptId);
	$("#deptName").val(deptName);
	loadAppList(deptId);
}
//获取部门对应的应用
function loadAppList(deptId){
	$("#app").empty();
	$("#app").append("<option value=\"\">---请选择应用---</option>");
	$.get("/schedule/app/list",{deptId:deptId}, function (data) {
		$("#app").empty();
		$("#app").append("<option value=\"\">---请选择应用---</option>");
		$.each(data.rows, function (index, item) {  
            var id = item.appNo; 
            var text = item.appName+"["+item.appNo+"]"; 
            $("#app").append("<option value='"+id+"'>"+text+"</option>");
        }); 
	});
}