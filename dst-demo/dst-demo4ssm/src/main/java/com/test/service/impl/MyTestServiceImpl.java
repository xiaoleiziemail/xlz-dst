package com.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.bo.MyTestDO;
import com.test.dao.BaseDBDAO;
import com.test.dao.MyTestDBDAO;
import com.test.service.MyTestService;

@Service("myTestService")
public class MyTestServiceImpl extends BaseServiceImpl<MyTestDO>
  implements MyTestService
{

  @Autowired
  private MyTestDBDAO dao;

  public List<MyTestDO> getMyTestList(Integer len,String item, Long fetchDataNumber,String tableName)
  {
    List<MyTestDO> list = this.dao.getMyTestList(len,item,fetchDataNumber,tableName);
    return list;
  }

  public int update(MyTestDO t,String tableName,String instance){
      return dao.update(t.getId(),tableName,instance);
  }
  
  protected BaseDBDAO<MyTestDO> getDAO()
  {
    return this.dao;
  }
}