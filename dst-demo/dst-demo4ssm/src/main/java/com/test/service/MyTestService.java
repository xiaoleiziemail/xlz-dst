package com.test.service;

import java.util.List;

import com.test.bo.MyTestDO;

public abstract interface MyTestService extends BaseService<MyTestDO>
{
  public abstract List<MyTestDO> getMyTestList(Integer len,String item, Long fetchDataNumber,String tableName);
  
  public abstract int update(MyTestDO t,String tableName,String instance);
}