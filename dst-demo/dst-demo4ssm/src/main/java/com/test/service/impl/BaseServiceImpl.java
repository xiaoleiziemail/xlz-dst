package com.test.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.bo.BaseDomain;
import com.test.dao.BaseDBDAO;
import com.test.service.BaseService;

public abstract class BaseServiceImpl<T extends BaseDomain> implements BaseService<T>{
	/**
	 * 日志
	 */
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	
	protected abstract BaseDBDAO<T> getDAO();
	protected void beforeSave(T entity){}
	protected void beforeUpdate(T entity){};
	protected void beforeDelete(String ids){};
	
	protected void afterSave(T entity){}
	protected void afterUpdate(T entity){};
	protected void afterDelete(String ids){};

	public T findById(Integer id) {
		return getDAO().findById(id);
	}
	
	public void update(T t) {
        beforeUpdate(t);
        getDAO().update(t);
        afterUpdate(t);
	}

    @Override
    public Integer save(T entity) {
        beforeSave(entity);
        getDAO().save(entity);
        afterSave(entity);
        return entity.getId();
    }

    @Override
    public void delete(String ids) {
        beforeDelete(ids);
        String[] strs = ids.split(",");
        for(String str : strs){
            getDAO().delete(new Integer(str));
        }
        afterDelete(ids);
    }
/*
    @Override
    public List<T> findByPage(List<FilterRule> filterRuleList, PageQuery pageQuery) {
        pageQuery.setTotalCount(getDAO().getTotalCount(filterRuleList));
        if(pageQuery.getTotalCount() <= 0){
        	return null;
        }
        return getDAO().findByPage(filterRuleList, pageQuery);
    }

    @Override
    public Integer getTotalCount(List<FilterRule> filterRuleList) {
        return getDAO().getTotalCount(filterRuleList);
    }*/
}
