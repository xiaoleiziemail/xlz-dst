package com.test.bo;

import java.io.Serializable;
import java.util.Date;

public class BaseDomain implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7554771532081356478L;
	
	protected Integer id;

	protected String createUser;

	protected Date createTime;

	protected String updateUser;

	protected Date updateTime;

	protected Boolean isActive = true;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
