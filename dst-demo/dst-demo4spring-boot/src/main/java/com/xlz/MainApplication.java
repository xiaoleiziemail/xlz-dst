package com.xlz;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.xlz.ds.DataSourceFactory;
import com.xlz.ds.PooledDataSource;
import com.xlz.ds.PooledDataSourceFactory;
import com.xlz.manager.DstManagerBeanFactory;
import com.xlz.util.Common;

@SpringBootApplication
public class MainApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}
	
	/**
	 * 初始化dst管理类
	 * @param dataSource
	 * @return
	 * @throws IOException
	 */
	@Bean
	public DstManagerBeanFactory dstInit(DataSource dataSource) throws IOException{
		DstManagerBeanFactory dstManager = new DstManagerBeanFactory();
		dstManager.setAppNo("springboot");
		dstManager.setDataSource(dataSource);
		return dstManager;
	}
	
	/**
	 * 初始化数据源，此处可以使用dbcp、c3p0等数据库连接池
	 * @return
	 */
	@Bean
	public DataSource dataSource() {
		DataSourceFactory dataSourceFactory = new PooledDataSourceFactory();
		Properties props = new Properties();
		dataSourceFactory.setProperties(props);
		PooledDataSource pooledDataSource = (PooledDataSource) dataSourceFactory.getDataSource();
		pooledDataSource.setDriver("com.mysql.jdbc.Driver");
		pooledDataSource.setUrl("jdbc:mysql://127.0.0.1:3306/dst?useUnicode=true&amp;characterEncoding=UTF-8");
		pooledDataSource.setUsername("test");
		pooledDataSource.setPassword("zhangll"); 
		pooledDataSource.setPoolMaximumActiveConnections(10);
		pooledDataSource.setPoolMaximumCheckoutTime(1000 * 3);
		pooledDataSource.setPoolPingQuery("select 1");
		pooledDataSource.setDefaultAutoCommit(false);
		return pooledDataSource;
	}
	
}
